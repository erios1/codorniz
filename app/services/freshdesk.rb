# Clase para recolectar informacion de Freshdesk
#
class Freshdesk

  # @return [TableroFreshdesk]
  attr_reader :tablero_freshdesk
  # @return [RegistroFreshdesk]
  attr_reader :registro_freshdesk
  attr_reader :agente_freshdesk
  attr_reader :contacto_freshdesk

  # @scope class

  def self.generate_data
    generate_agent
    generate_companies
    generate_contacts
    generate_groups
    generate_ticket
  end
  def self.generate_agent
    # para que se pueda conseguir la informacion
    author = "JsDMRSRRQ2SAws4IFeVh"
    # Recurso 
    resource = RestClient::Resource.new('https://buk.freshdesk.com/api/v2', user: author, password: 'X')
    per_page = 100
    page = 1
    results_agents = nil
    agentes = []
    while results_agents.nil? || per_page == results_agents&.count
      agents = "agents?per_page=#{per_page}&page=#{page}"
      conexion_agents = resource[agents].get
      results_agents = JSON.parse(conexion_agents.to_str)
      results_agents.each do |a|
        id = a["id"]
        email = a["contact"]["email"]
        estado = a["contact"]["active"] ? "Activo" : "Inactivo"
        nombre = a["contact"]["name"]
        telefono = a["contact"]["mobile"]
        tipo = a["type"]
        titulo_trabajo = a["contact"]["job_title"]
        agentes << AgenteFreshdesk.new(id: id, email: email, estado: estado, nombre: nombre, telefono: telefono, tipo: tipo, titulo_trabajo: titulo_trabajo)
      end
      page += 1
    end
    AgenteFreshdesk.import!(agentes)
  end

  def self.generate_groups
    # para que se pueda conseguir la informacion
    author = "JsDMRSRRQ2SAws4IFeVh"
    # Recurso 
    resource = RestClient::Resource.new('https://buk.freshdesk.com/api/v2', user: author, password: 'X')
    per_page = 100
    page = 1
    results_groups = nil
    grupos = []
    hash_group = {}
    while results_groups.nil? || per_page == results_groups&.count
      groups = "admin/groups?per_page=#{per_page}&page=#{page}"
      conexion_groups = resource[groups].get
      results_groups = JSON.parse(conexion_groups.to_str)
      results_groups.each do |g|
        id = g["id"]
        descripcion = g["description"]
        nombre = g["name"]
        tipo = g["type"]
        ids = g["agent_ids"]
        grupos << GrupoFreshdesk.new(id: id, descripcion: descripcion, nombre: nombre, tipo: tipo)
        hash_group = hash_group.merge({id => ids})
      end
      page += 1
    end
    GrupoFreshdesk.import!(grupos)
    hash_group.each do |k, v|
      AgenteFreshdesk.where(id: v).update_all(grupo_freshdesk_id: k)
    end
  end

  def self.generate_ticket
    # para que se pueda conseguir la informacion
    author = "JsDMRSRRQ2SAws4IFeVh"
    # Recurso 
    resource = RestClient::Resource.new('https://buk.freshdesk.com/api/v2', user: author, password: 'X')
    results_tickets = nil
    per_page = 100
    page = 1
    tickets = []
    fecha = Time.zone.today
    TableroFreshdesk.create!(generacion: fecha, nombre: "Freshdesk #{fecha.year} #{fecha.month}", pais: "Chile", registros: 0)
    tablero = TableroFreshdesk.last
    while results_tickets.nil? || per_page == results_tickets&.count
      query_tickets = "tickets?per_page=#{per_page}&page=#{page}"
      conexion_tickets = resource[query_tickets].get
      results_tickets = JSON.parse(conexion_tickets.to_str)
      results_tickets.each do |res|
        id = res["id"]
        agente = res["responder_id"]
        bloqueante = res["is_escalated"]
        equipo = res["group_id"]
        estado = search_status(res["status"])
        informante = res["requester_id"]
        limite_primera_respuesta_date = res["fr_due_by"]
        prioridad = search_priority(res["priority"])
        resolved_date = res["due_by"]
        tipo = res["type"]
        titulo = res["subject"]
        url = res["custom_fields"]["cf_cliente"]
        created_at = res["created_at"]
        updated_at = res["updated_at"]
        cliente = res["custom_fields"]["cf_cliente"]   
        tickets << RegistroFreshdesk.new(id: id, agente_id: agente, bloqueante: bloqueante, equipo_id: equipo, estado: estado, informante_id: informante, 
          limite_primera_respuesta_date: limite_primera_respuesta_date, prioridad: prioridad, resolved_date: resolved_date, tipo: tipo, titulo: titulo, url: url, created_at: created_at, 
          updated_at: updated_at, tablero_freshdesk_id: tablero.id, cliente: cliente)
      end
      page += 1
    end
    RegistroFreshdesk.import!(tickets)
    tablero.update!(registros: RegistroFreshdesk.where(tablero_freshdesk_id: tablero.id).count)
  end

  def self.generate_contacts
    #tokens de autenticación para freshdesk
    author = "JsDMRSRRQ2SAws4IFeVh"
    resource = RestClient::Resource.new('https://buk.freshdesk.com/api/v2', user: author, password: 'X')
    #parámetros por página para conseguir la información
    per_page = 100
    page = 1
    contacts_result = nil
    contacts = []
    while contacts_result.nil? || per_page == contacts_result&.count
      contacts_query = "contacts?per_page=#{per_page}&page=#{page}"
      contacts_conn = resource[contacts_query].get
      contacts_result = JSON.parse(contacts_conn.to_s)
      contacts_result.each do |c|
        id = c["id"]
        email = c["email"]
        status = c["active"]? "Activo" : "Inactivo"
        nombre = c["name"]
        contacts << ContactoFreshdesk.new(id: id, nombre: nombre, estado: status, email: email)
      end
      page += 1
    end
    ContactoFreshdesk.import!(contacts)
  end

  def self.generate_companies
    #tokens de autenticación para freshdesk
    author = "JsDMRSRRQ2SAws4IFeVh"
    resource = RestClient::Resource.new('https://buk.freshdesk.com/api/v2', user: author, password: 'X')
    #parámetros por página para conseguir la información
    per_page = 100
    page = 1
    company_result = nil
    company = []
    while company_result.nil? || per_page == company_result&.count
      company_query = "companies?per_page=#{per_page}&page=#{page}"
      company_conn = resource[company_query].get
      company_result = JSON.parse(company_conn.to_s)
      company_result.each do |c|
        id = c["id"]
        name = c["name"]
        country = c["account_tier"]
        domain = c["domains"].empty? ? "Sin dominio" : c["domains"].first&.to_s
        company << CompaniaFreshdesk.new(id: id, country: country, domain: domain, name: name)
      end
      page += 1
    end
    CompaniaFreshdesk.import!(company)
  end

  def self.generate_ticket_faltantes
    # para que se pueda conseguir la informacion
    author = "JsDMRSRRQ2SAws4IFeVh"
    # Recurso 
    resource = RestClient::Resource.new('https://buk.freshdesk.com/api/v2', user: author, password: 'X')
    tickets = []
    fecha = Time.zone.today
    TableroFreshdesk.create!(generacion: fecha, nombre: "Freshdesk #{fecha.year} #{fecha.month}", pais: "Chile", registros: 0)
    tablero = TableroFreshdesk.last
    id_mayor = RegistroFreshdesk.last.id
    while id_mayor > 0
      query_tickets = "tickets?#{id_mayor}"
      begin
        conexion_tickets = resource[query_tickets].get
        results_tickets = JSON.parse(conexion_tickets.to_str)
        results_tickets.each do |res|
          id = res["id"]
          agente = res["responder_id"]
          bloqueante = res["is_escalated"]
          equipo = res["group_id"]
          estado = search_status(res["status"])
          informante = res["requester_id"]
          limite_primera_respuesta_date = res["fr_due_by"]
          prioridad = search_priority(res["priority"])
          resolved_date = res["due_by"]
          tipo = res["type"]
          titulo = res["subject"]
          url = res["custom_fields"]["cf_url_cliente"]
          created_at = res["created_at"]
          updated_at = res["updated_at"]    
          cliente = res["custom_fields"]["cf_cliente"]   
          tickets << RegistroFreshdesk.new(id: id, agente_id: agente, bloqueante: bloqueante, equipo_id: equipo, estado: estado, informante_id: informante, 
            limite_primera_respuesta_date: limite_primera_respuesta_date, prioridad: prioridad, resolved_date: resolved_date, tipo: tipo, titulo: titulo, url: url, created_at: created_at, 
            updated_at: updated_at, tablero_freshdesk_id: tablero.id, cliente: cliente)
        end
        id_mayor -= 1
      rescue RestClient::ExceptionWithResponse => e
        id_mayor -= 1
        next
      end
    end
    RegistroFreshdesk.import!(tickets)
    tablero.update!(registros: RegistroFreshdesk.where(tablero_freshdesk_id: tablero.id).count)
  end
  private
    def self.search_status status
      case status
      when 2
        "Abierto"
      when 3
        "Pendiente"
      when 4
        "Resuelto"
      when 5
        "Cerrado"
      end
    end

    def self.search_priority priority
      case priority
      when 1
        "Baja"
      when 2
        "Media"
      when 3
        "Alta"
      when 4
        "Urgente"
      end
    end

end

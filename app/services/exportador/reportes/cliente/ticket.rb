class Exportador::Reportes::Cliente::Ticket
  def self.generate_file
    book = Exportador::BaseXlsx.crear_libro
    book.worksheets = []
    sheet = Exportador::BaseXlsx.crear_hoja book, "Tickets Asociados"
    titulos = ["Cliente", "Criticidad","Total", "Total Resuelto", "Urgente", "Urgente Resueltos", "Alta", "Alta Resueltos", "Media", "Media Resueltos", "Bajo", "Bajo Resuelto"]
    Exportador::BaseXlsx.escribir_celdas sheet, [titulos], offset: 0
    clientes = RegistroTableroMonday.all.uniq(&:url)
    today = Time.zone.today
    mes = today.beginning_of_month
    excel_data = clientes.map do |c|
      tickets = c.ticket_freshdesks.actualizado_entre(mes, mes.end_of_month)
      [
        c.nombre,
        c.criticidad,
        tickets.count,
        tickets.terminados.count,
        tickets.urgentes.count,
        tickets.urgentes.terminados.count,
        tickets.altos.count,
        tickets.altos.terminados.count,
        tickets.medios.count,
        tickets.medios.terminados.count,
        tickets.bajos.count,
        tickets.bajos.terminados.count,
      ]
    end
    Exportador::BaseXlsx.escribir_celdas sheet, excel_data, offset: 1
    Exportador::BaseXlsx.autofit sheet, titulos
    book.write("/home/emilio/code/cliente_ticket-#{mes}-#{today}.xlsx")
  end
end
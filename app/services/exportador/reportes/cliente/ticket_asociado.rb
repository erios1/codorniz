class Exportador::Reportes::Cliente::TicketAsociado
  def self.generate_file
    book = Exportador::BaseXlsx.crear_libro
    book.worksheets = []
    sheet = Exportador::BaseXlsx.crear_hoja book, "Tickets Asociados"
    titulos = ["Cliente", "Criticidad", "URL", "Titulo Ticket", "Prioridad", "Informante", "Equipo Asociado", "Agente Asociado"]
    Exportador::BaseXlsx.escribir_celdas sheet, [titulos], offset: 0
    clientes = RegistroTableroMonday.all.uniq(&:url)
    excel_data = []
    today = Time.zone.today
    mes = today.beginning_of_month
    clientes.each do |c|
      next if c.ticket_freshdesks.empty?
      data = c.ticket_freshdesks.actualizado_entre(mes, mes.end_of_month).map do |r|
        [
          c.nombre,
          c.criticidad,
          c.url,
          r.titulo,
          r.prioridad,
          r.informante&.nombre,
          r.equipo&.nombre,
          r.agente&.nombre,
        ]
      end
      excel_data |= data
    end
    Exportador::BaseXlsx.escribir_celdas sheet, excel_data, offset: 1
    Exportador::BaseXlsx.autofit sheet, titulos
    book.write("/home/emilio/code/cliente_ticket_asociados-#{mes}-#{today}.xlsx")
  end
end
class Exportador::Reportes::Agente::TicketAsociado
  def self.generate_file
    book = Exportador::BaseXlsx.crear_libro
    book.worksheets = []
    sheet = Exportador::BaseXlsx.crear_hoja book, "Tickets Asociados"
    titulos = ["Agente", "Nombre Ticket", "URL Cliente", "Prioridad", "Informante", "Equipo"]
    Exportador::BaseXlsx.escribir_celdas sheet, [titulos], offset: 0
    today = Time.zone.today
    mes = today.beginning_of_month
    registros = RegistroFreshdesk.actualizado_entre(mes, mes.end_of_month)
    excel_data = registros.map do |r|
      [
        r.agente&.nombre,
        r.titulo,
        r.cliente_url,
        r.prioridad,
        r.informante&.nombre,
        r.equipo&.nombre
      ]
    end
    Exportador::BaseXlsx.escribir_celdas sheet, excel_data, offset: 1
    Exportador::BaseXlsx.autofit sheet, titulos
    book.write("/home/emilio/code/agente_ticket_asociados-#{mes}-#{today}.xlsx")
  end
end
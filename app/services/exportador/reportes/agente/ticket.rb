class Exportador::Reportes::Agente::Ticket
  def self.generate_file
    book = Exportador::BaseXlsx.crear_libro
    book.worksheets = []
    sheet = Exportador::BaseXlsx.crear_hoja book, "Tickets Asociados"
    titulos = ["Agente", "Total", "Total Resuelto", "Urgente", "Urgente Resueltos", "Alta", "Alta Resueltos", "Media", "Media Resueltos", "Bajo", "Bajo Resuelto"]
    Exportador::BaseXlsx.escribir_celdas sheet, [titulos], offset: 0
    agentes = AgenteFreshdesk.all
    Time.zone.today
    mes = today.beginning_of_month
    excel_data = agentes.map do |a|
      registros = a.registro_freshdesks.actualizado_entre(mes, mes.end_of_month)
      [
        a.nombre,
        registros.count,
        registros.terminados.count,
        registros.urgentes.count,
        registros.terminados.urgentes.count,
        registros.altos.count,
        registros.terminados.altos.count,
        registros.medios.count,
        registros.terminados.medios.count,
        registros.bajos.count,
        registros.terminados.bajos.count,
      ]
    end
    Exportador::BaseXlsx.escribir_celdas sheet, excel_data, offset: 1
    Exportador::BaseXlsx.autofit sheet, titulos
    book.write("/home/emilio/code/agente_ticket-#{mes}-#{today}.xlsx")
  end
end
class Exportador::Reportes::Agente::Efectividad
  def self.generate_file
    book = Exportador::BaseXlsx.crear_libro
    book.worksheets = []
    sheet = Exportador::BaseXlsx.crear_hoja book, "Efectividad"
    titulos = ["Agente", "Email Agente", "Tickets Asignado", "Tickets Terminado", "MR Creados", "MR Mergados"]
    Exportador::BaseXlsx.escribir_celdas sheet, [titulos], offset: 0
    agentes = AgenteFreshdesk.all
    today = Time.zone.today.to_date
    mes = today.beginning_of_month
    excel_data = agentes.map do |a|
      autor = a.usuario_gitlab&.registro_authors&.actualizado_entre(mes.beginning_of_month, mes.end_of_month)
      registro_freshdesks = a.registro_freshdesks.actualizado_entre(mes, mes.end_of_month)
      [
        a.nombre,
        a.email,
        registro_freshdesks.count,
        registro_freshdesks.terminados.count,
        autor&.count,
        autor&.where&.not(mergeado: nil)&.count,
      ]
    end
    Exportador::BaseXlsx.escribir_celdas sheet, excel_data, offset: 1
    Exportador::BaseXlsx.autofit sheet, titulos
    book.write("/home/emilio/code/agente_efectividad-#{mes}-#{today}.xlsx")
  end
end
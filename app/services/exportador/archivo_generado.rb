module Exportador
  class ArchivoGenerado
    include ActiveModel::Model

    attr_accessor :contenido
    attr_accessor :mime
    attr_accessor :extension
    attr_accessor :name
    attr_accessor :extra_attributes
  end
end

module Exportador
  #Clase base que hace uso de la gema RubyXl para poder generar archivos en formato xlsx
  class BaseXlsx
    INVALID_CHARS_REGEX = Regexp.union(%w{[ ] * / \\ ? :}).freeze

    require 'rubyXL'
    require 'rubyXL/convenience_methods/cell'
    require 'rubyXL/convenience_methods/color'
    require 'rubyXL/convenience_methods/workbook'
    require 'rubyXL/convenience_methods/worksheet'
    require 'rubyXL/convenience_methods/font'

    def self.crear_libro
      RubyXL::Workbook.new
    end

    # Abre un libro de cálculo .xlsx local o remoto.
    #
    # @param [Pathname|CarrierWave::SanitizedFile|String] file Ruta al archivo o cualquier objeto que responda a .read
    #
    # @return [RubyXL::Workbook] Libro de cálculo
    #
    # TODO: Separar apertura de libros: local, remoto
    #
    def self.abrir_libro file
      content = file.respond_to?(:read) ? file.read : FileHelper.read(file)
      ::RubyXL::Parser.parse_buffer content
    rescue Zip::Error => e
      raise Xlsx::Error, "Error al abrir archivo - #{e.message}"
    end

    #
    # Escribe celdas de una sección una hoja de cálculo
    #
    # @param [RubyXL::Worksheet] sheet Hoja de cálculo
    # @param [Exportador::Xlsx::Section] section Sección a escribir en la hoja.
    # @return [void]
    #
    def self.escribir_seccion sheet, section
      section.matrix.each do |coord, value|
        next if coord.include? nil
        row_index = coord[0] + section.row_offset
        column_index = coord[1] + section.column_offset
        sheet[row_index][column_index].raw_value = value
      end
    end

    #
    # Copia el contenido de una hoja de cálculo a otra
    #
    # @param [Spreadsheet::Excel::Worksheet] sheet1 Hoja de cálculo fuente
    # @param [Spreadsheet::Excel::Worksheet] sheet2 Hoja de cálculo destino
    #
    # @return [void]
    #
    def self.copiar_contenido_hoja sheet1, sheet2
      sheet1.each_with_index do |row, i|
        num_cols = row.size
        (0...num_cols).each do |j|
          sheet2[i, j] = row[j]
        end
      end
    end

    #
    # Crea una hoja en un libro de cálculo
    #
    # @param [RubyXL::Worksheet] book Libro de cálculo
    # @param [String] name Nombre de la hoja de cálculo
    #
    # @return [void]
    #
    def self.crear_hoja book, name
      book.add_worksheet(sanitizar_nombre(name))
    end

    #
    # Escribe celdas en la hoja de cálculo proporcionada
    #
    # @param [RubyXL::Worksheet] sheet Hoja de cálculo
    # @param [Array<Array>] cells Los valores que tendrán las celdas
    #
    # @return [void]
    #
    def self.escribir_celdas sheet, cells, offset: 1, number_format: '#,##0.00', true_value: 1, false_value: 0
      cells.each_with_index do |row, row_index|
        row.each_with_index do |cell_value, column_index|
          c = sheet.add_cell(row_index + offset, column_index)
          case cell_value
          when ActiveSupport::TimeWithZone, DateTime
            c.set_number_format 'yyyy-mm-dd HH:MM:ss'
            # Si ponemos el valor directo, excel nos convierte de la zona local a UTC
            # (por ejemplo, de 4 de la tarde en chile a 7 de la tarde UTC)
            # Asi que con esto quitamos la información de zona horaria
            cell_value = cell_value.asctime.to_datetime
          when Date
            c.set_number_format 'yyyy-mm-dd'
          when Numeric
            c.set_number_format number_format if number_format.present?
          when TrueClass, FalseClass
            cell_value = cell_value ? true_value : false_value
          end
          c.change_contents cell_value
        end
      end
    end

    #
    # Crea una hoja en un libro de cálculo con la cabecera y celdas proporcionadas
    #
    # @param [RubyXL::Workbook] book Libro de cálculo
    # @param [String] sheet_name Nombre de la nueva hoja de cálculo
    # @param [Array] header Nombres para las columnas de la cabecera
    # @param [Array[Array]] cells Valores de las celdas a insertar
    #
    # @return [void]
    #
    def self.escribir_hoja book, sheet_name, header, cells
      new_sheet = Exportador::BaseXlsx.crear_hoja book, sheet_name
      Exportador::BaseXlsx.crear_encabezado new_sheet, header
      Exportador::BaseXlsx.escribir_celdas new_sheet, cells
    end

    #
    # 'Cierra' un libro de cálculo. Si está vacío retorna una hoja vacía en el libro, caso
    #  contrario devuelve su contenido
    #
    # @param [RubyXL::Workbook] book Libro de cálculo
    #
    # @return [Exportador::ArchivoGenerado] El archivo generado
    #
    def self.cerrar_libro book
      crear_hoja_vacia(book) if book.worksheets.empty?
      book.worksheets.each { |w| w.sheet_name = sanitizar_nombre(w.sheet_name) }
      mime = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      str = book.stream.string
      # xlsx es binario porque está comprimido, asi que no tiene sentido intentar
      # interpretarlo como utf-8. Si no hacemos esto la funcion blank? se cae
      # porque trata de parsear el utf-8 para saber si hay caracteres en blanco
      str.force_encoding 'BINARY'
      Exportador::ArchivoGenerado.new(contenido: str,
                                      mime: mime,
                                      extension: '.xlsx')
    end

    #
    # Agrega una hoja al libro de cálculo con 'No hay datos' en la primera celda.
    #
    # @param [RubyXL::Workbook] libro Libro de cálculo.
    #
    def self.crear_hoja_vacia book
      new_sheet = book.add_worksheet('Hoja Vacía')
      new_sheet.add_cell 0, 0, 'No hay datos'
    end

    # Busca una hoja vacía y le asigna el nombre a la hoja,
    # caso contrario crea una hoja vacía.
    #
    # @param [RubyXL::Workbook] book Libro de cálculo.
    # @param [String] name Nombre para la hoja de cálculo
    #
    # @return [RubyXL::Worksheet] La hoja de cálculo
    def self.buscar_hoja_disponible book, name
      sheet = book.worksheets.find{ |s| s.sheet_data.rows.empty? }
      sheet.sheet_name = name if sheet
      sheet || crear_hoja(book, name)
    end

    #
    # Agrega un encabezado a una hoja de cálculo
    #
    # @param [RubyXL::Worksheet] hoja Hoja de cálculo para insertar encabezado
    # @param [Array] columnas Nombres para las columnas de la cabecera
    #
    # @return [void]
    #
    def self.crear_encabezado sheet, header, offset = 0
      header.each_with_index do |column_name, index|
        sheet.add_cell(offset, index, column_name)
      end
      sheet.change_row_bold(offset, true)
    end

    def self.crear_encabezado_hoja sheet, options = {}
      cells = []
      # Encabezados opcionales
      cells.push(['Estado: ' + options[:estado]]) if options[:estado].present?
      cells.push(['Libro: ' + options[:titulo]]) if options[:titulo].present?
      cells.push(['Empresa: ' + options[:empresa].nombre + ' (' + options[:empresa].rut.humanize + ')']) if options[:empresa].present?
      cells.push(['Periodo: ' + I18n.l(options[:variable].mes, format: "%B %Y").capitalize]) if options[:variable].present?
      # Agregar Fecha de Generación Siempre
      fecha_generacion = Time.zone.now.strftime("%d/%m/%Y a las %I:%M%p")
      cells.push(['Fecha Generación: ' + fecha_generacion.to_s])

      escribir_celdas sheet, cells, offset: 0
      0.upto(options.size) { |row| sheet.change_row_bold(row, true) }
    end

    #
    # Ajusta el ancho de las columnas de la hoja de cálculo con su texto
    #
    # @param [RubyXL::Worksheet] sheet Hoja de cálculo
    # @param [Array<Array>] cells Los valores que tendrán las celdas
    # @param [Integer] limit El número de filas máximo a mirar
    # @return [void]
    #
    def self.autofit sheet, cell, limit: 50
      index = -1
      cell.each do |object|
        size = object.size
        max_sizes = 100
        max_sizes = size if size < max_sizes
        index += 1
        sheet.change_column_width(index, max_sizes)
      end
    end

    def self.sanitizar_nombre(nombre)
      nombre.gsub(INVALID_CHARS_REGEX, ' ')
    end

    # Elimina todas las columnas vacías en los datos
    # Vacío se define como 0, string vacío, nil o el string '0'
    #
    # @param [Array<Array>] data_rows La matriz de datos
    # @param [Array] titles Los headers de los datos
    #
    # @return [Array<Array>] Un arreglo donde el primer elemento son las filas con datos, y el segundo son los headers. (Es decir, igual a los parámetros pero filtrados)
    #
    # @example
    #
    #   data_headers = ['Vacio', 'No Vacio', 'No Vacio 2']
    #   data_rows = [[0, 1, 1]]
    #   filtered_rows, filtered_headers = compact_empty_rows(data_rows, data_headers)
    #   filtered_rows # => [[1, 1]]
    #   filtered_headers # => ['No Vacio', 'No Vacio 2']
    def self.compact_empty_columns data_rows, titles
      included_columns = data_rows.each_column.map do |column_values|
        column_values.any?{|val| !val.nil? && val.present? && val != 0 && val != "0" && val != "0.00"}
      end

      filter = proc do |row|
        row.select!.with_index{|_, i| included_columns[i] }
      end
      output_titles, *output_rows = ([titles] + data_rows).map!(&filter)
      [output_rows, output_titles]
    end
  end
end

# Clase para recolectar informacion de Gitlab
#
class Gitlab

  # @return [TableroGitlab]
  attr_reader :tablero_gitlab
  # @return [RegistroGitlab]
  attr_reader :registro_gitlab
  attr_reader :usuario_gitlab

  # @scope class

  def self.generate_data
    generate_user
    generate_mr
  end
  def self.generate_mr
    # para que se pueda conseguir la informacion
    author = "E6iGH3QegACtzHgwATan"
    # Recurso 
    resource = RestClient::Resource.new('https://gitlab.com/api/v4')
    header = {"Private-Token": author}
    per_page = 100
    page = 1
    fecha = Time.zone.today
    TableroGitlab.create!(generacion: Time.zone.today, nombre: "Tablero Gitlab #{fecha.year} #{fecha.month}", registros: 0)
    tablero = TableroGitlab.last
    results_mr = nil
    merges = []
    while results_mr.nil? || per_page == results_mr&.count
      mr = "/projects/3310437/merge_requests?&per_page=#{per_page}&page=#{page}&scope=all"
      conexion_mr = resource[mr].get(header)
      results_mr = JSON.parse(conexion_mr.to_str)
      results_mr.each do |rm|
        creado = rm["created_at"]
        descripcion = rm["description"]
        estado = ESTADO[rm["state"].to_sym]
        mergeado = rm["merged_at"]
        nombre_mr = rm["title"]
        nombre_rama = rm["source_branch"]
        rama_destino = rm["target_branch"]
        revisores = reviewers(rm)
        web_url = rm["web_url"]
        author_id = rm["author"]&.dig("id")
        merge_by_id = rm["merged_by"]&.dig("id")
        tablero_gitlab_id = tablero.id
        merges << RegistroGitlab.new(
          creado: creado, descripcion: descripcion, estado: estado, mergeado: mergeado, nombre_mr: nombre_mr, nombre_rama: nombre_rama,
          rama_destino: rama_destino, revisores: revisores, web_url: web_url, author_id: author_id, merge_by_id: merge_by_id, tablero_gitlab_id: tablero_gitlab_id,
        )
      end
      page += 1
    end
    RegistroGitlab.import!(merges)
    tablero.update!(registros: RegistroGitlab.where(tablero_gitlab_id: tablero.id).count)
  end

  def self.generate_user
    # para que se pueda conseguir la informacion
    author = "E6iGH3QegACtzHgwATan"
    # Recurso 
    resource = RestClient::Resource.new('https://gitlab.com/api/v4')
    header = {"Private-Token": author}
    per_page = 100
    page = 1
    results_user = nil
    users = []
    while results_user.nil? || per_page == results_user&.count
      user = "projects/3310437/users?&per_page=#{per_page}&page=#{page}&exclude_internal=true"
      conexion_user = resource[user].get(header)
      results_user = JSON.parse(conexion_user.to_str)
      results_user.each do |u|
        id = u["id"]
        nombre = u["name"]
        nombre_usuario = u["username"]
        estado = u["state"] == "active" ? "Activo" : "Inactivo"
        web_url = u["web_url"]
        users << UsuarioGitlab.new(id: id, nombre: nombre, nombre_usuario: nombre_usuario, estado: estado, web_url: web_url)
      end
      page += 1
    end
    UsuarioGitlab.import!(users)
  end

  ESTADO = {"opened": "abierto", "closed": "cerrado", "merged": "mergeado"}

  private
    def self.reviewers rm
      ids = ""
      rm["reviewers"].each do |r|
        ids += r == rm["reviewers"] ? r["id"] : ", #{r["id"]}"
      end
      ids
    end
end

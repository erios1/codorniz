# Clase para recolectar informacion de Jira
#
class Jira

  # @return [TableroMonday]
  attr_reader :tablero_jira
  # @return [RegistroTableroMonday]
  attr_reader :proyecto_jira

  # @scope class

  def self.generate_data
    generate_project
    generate_issues_type
    generate_issue
  end
  def self.generate_project
    # para que se pueda conseguir la informacion
    author = "l7cYLFwZ1PH8Y6ONA7etD0F8"
    # Recurso 
    resource = RestClient::Resource.new('https://buk.atlassian.net/rest/api/2',user: "dvaldivia@buk.cl", password: author)
    proyectos = []
    project = "project"
    conexion = resource[project].get
    results = JSON.parse(conexion.to_str)
    results.each do |p|
      id = p["id"].to_i
      codigo = p["key"]
      nombre = p["name"]
      type_key = p["projectTypeKey"]
      proyectos << ProyectoJira.new(id: id, codigo: codigo, nombre: nombre, type_key: type_key)
    end
    ProyectoJira.import!(proyectos)
  end

  # para traer todos los registros (costoso)
  def self.generate_issue
    # para que se pueda conseguir la informacion
    author = "l7cYLFwZ1PH8Y6ONA7etD0F8"
    # Recurso 
    resource = RestClient::Resource.new('https://buk.atlassian.net/rest/api/2',user: "dvaldivia@buk.cl", password: author)
    proyectos = ProyectoJira.all
    issues = []
    id_issue = []
    TableroJira.create!(generacion: Time.zone.today, nombre: "Tablero General", pais: "Chile", registros: 0)
    tablero = TableroJira.last
    proyectos.each do |p|
      issues_project = "search?jql=project=#{p.id}+order+by+duedate&fields=id,key&maxResults=10000"
      conexion = resource[issues_project].get
      results = JSON.parse(conexion.to_str)
      results["issues"].each do |i|
        id_issue << i["id"]
      end
    end
    id_issue.each do |iu|
      issues_project = "issue/#{iu}"
      conexion = resource[issues_project].get
      results = JSON.parse(conexion.to_str)
      fields = results["fields"]
      codigo = results["key"]
      resolucion = "Deseable"
      prioridad = fields["priority"]&.dig("name")
      responsable = fields["assignee"]&.dig("displayName")
      status = fields["status"]&.dig("name")
      informante = fields["creator"]&.dig("displayName")
      issue_type_id = fields["issuetype"]&.dig("id")
      project_id = fields["project"]&.dig("id")
      nombre = fields["summary"]
      fecha_termino = fields["statuscategorychangedate"]
      fecha_inicio = fields["created"]
      tablero_jira_id = tablero.id
      issues << RegistroJira.new(
        codigo: codigo, resolucion: resolucion, prioridad: prioridad, responsable: responsable,
        status: status, informante: informante, issue_type_id: issue_type_id, project_id: project_id, nombre: nombre,
        fecha_termino: fecha_termino, fecha_inicio: fecha_inicio, tablero_jira_id: tablero_jira_id
      )
    end
    RegistroJira.import!(issues)
    tablero.update!(registros: RegistroJira.where(tablero_jira_id: tablero.id).count)
  end

  def self.generate_issues_type
    author = "l7cYLFwZ1PH8Y6ONA7etD0F8"
    # Recurso 
    resource = RestClient::Resource.new('https://buk.atlassian.net/rest/api/2/',user: "dvaldivia@buk.cl", password: author)
    issues_type = []
    conexion = resource['issuetype'].get
    results = JSON.parse(conexion.to_str)
    results.each do |p|
      issue_type_id = p['id']
      descripcion = p['description']
      nombre = p['name']
      subtask = p['subtask']
      tipo = p['scope']&.dig('type')
      project_id = p['scope']&.dig('project')&.dig('id')
      issues_type << IssuesTypeJira.new(id: issue_type_id, description: descripcion, name: nombre, subtask: subtask, tipo: tipo, project_id: project_id)
    end
    IssuesTypeJira.import!(issues_type)
  end
end

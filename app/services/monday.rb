# Comando para recolectar informacion de Monday
#
# Genera un tablero monday y sus registros
#
class Monday

  # @return [TableroMonday]
  attr_reader :tablero_monday
  # @return [RegistroTableroMonday]
  attr_reader :registro_tablero_monday

  # @scope class

  def self.generate
    resource = RestClient::Resource.new 'https://api.monday.com/v2'
    author = "eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjExMDY0NjY2NywidWlkIjoxMjY5MDczOCwiaWFkIjoiMjAyMS0wNS0xOVQxNjo1Nzo1My41MjlaIiwicGVyIjoibWU6d3JpdGUiLCJhY3RpZCI6NTY4Mjg3OCwicmduIjoidXNlMSJ9.YWC25-1o-rYtLFFWGU-snmfs4PvEDU5yLKAe0fwwZbg"
    query = "{ boards (ids: 466188439){name items {name column_values (ids: [n__emp_ etiquetas1 nombre_pagina proyecto jefe_proyecto1 jefe_proyecto0 kam4 estado n_meros_17 modulos fecha_inicio fecha_termino texto_1 texto_largo]){id text}}} }"
    header = { "Content-Type": "application/json", 'Authorization': author, params: { query: query } }
    response = resource.get(header)
    results_query = JSON.parse(response.to_str)
    # tomar nombre del tablero master clientes chile
    nombre = results_query["data"]["boards"][0]["name"]
    TableroMonday.create!(generacion: Time.zone.today, nombre: nombre, pais: "Chile", registros: 0)
    tablero = TableroMonday.last
    items = results_query["data"]["boards"][0]["items"]
    registros = []
    items.each do |item|
      nombre = item["name"]
      n_empleados, etiqueta_tamanno, url, estado, jp, jp_backup, agente_c, critico, etapa, modulos, fecha_inicio, fecha_termino, contraparte, email_contraparte = nil
      item["column_values"].each do |cl|
        if cl["id"] == "n__emp_"
          n_empleados = cl["text"]
        elsif cl["id"] == "etiquetas1"
          etiqueta_tamanno = cl["text"]
        elsif cl["id"] == "nombre_pagina"
          url = cl["text"]
        elsif cl["id"] == "proyecto"
          estado = cl["text"]
        elsif cl["id"] == "jefe_proyecto1"
          jp = cl["text"]
        elsif cl["id"] == "jefe_proyecto0"
          jp_backup = cl["text"]
        elsif cl["id"] == "kam4"
          agente_c = cl["text"]
        elsif cl["id"] == "estado"
          critico = cl["text"]
        elsif cl["id"] == "n_meros_17"
          etapa = cl["text"]
        elsif cl["id"] == "modulos"
          modulos = cl["text"]
        elsif cl["id"] == "fecha_inicio"
          fecha_inicio = cl["text"]
        elsif cl["id"] == "fecha_termino"
          fecha_termino = cl["text"]
        elsif cl["id"] == "texto_1"
          contraparte = cl["text"]
        elsif cl["id"] == "texto_largo"
          email_contraparte = cl["text"]
        end
      end
      duracion = fecha_inicio.present? ? (((fecha_termino.presence || Time.zone.today).to_time - fecha_inicio.to_time)/1.month.second).round(1) : "N/A"
      registros << RegistroTableroMonday.new(nombre: nombre, n_empleados: n_empleados, tamanno: etiqueta_tamanno, url: url, estado: estado, jefe_proyecto: jp, jefe_proyecto_backup: jp_backup,
        agente_comercial: agente_c, criticidad: critico, etapa: etapa, modulos: modulos, fecha_inicio: fecha_inicio,
        fecha_termino: fecha_termino, contraparte: contraparte, email_contraparte: email_contraparte, tablero_monday_id: tablero.id, duracion: duracion.to_s + " meses")
    end
    RegistroTableroMonday.import! registros
    tablero.update!(registros: RegistroTableroMonday.where(tablero_monday_id: tablero.id).count)
  end
end

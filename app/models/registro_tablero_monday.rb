# == Schema Information
#
# Table name: registro_tablero_mondays
#
#  id                   :bigint           not null, primary key
#  agente_comercial     :text
#  contraparte          :text
#  criticidad           :text
#  duracion             :text
#  email_contraparte    :text
#  estado               :text
#  etapa                :text
#  fecha_inicio         :date
#  fecha_termino        :date
#  jefe_proyecto        :text
#  jefe_proyecto_backup :text
#  modulos              :text
#  n_empleados          :integer
#  nombre               :text
#  tamanno              :text
#  url                  :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  tablero_monday_id    :integer
#
# Indexes
#
#  index_rtm_on_tablero_monday_id  (tablero_monday_id)
#
class RegistroTableroMonday < ApplicationRecord
  belongs_to :tablero_monday

  def ticket_freshdesks
    RegistroFreshdesk.where(cliente: url)
  end
end

# == Schema Information
#
# Table name: registro_gitlabs
#
#  id                :bigint           not null, primary key
#  creado            :date
#  descripcion       :text
#  estado            :text
#  mergeado          :date
#  nombre_mr         :text
#  nombre_rama       :text
#  rama_destino      :text
#  revisores         :text
#  web_url           :text
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  author_id         :bigint
#  merge_by_id       :bigint
#  tablero_gitlab_id :bigint
#
# Indexes
#
#  index_rg_on_tablero_gitlab_id  (tablero_gitlab_id)
#
class RegistroGitlab < ApplicationRecord

  #relaciones
  belongs_to :tablero_gitlab
  belongs_to :author, class_name: 'UsuarioGitlab', optional: true
  belongs_to :merge_by, class_name: 'UsuarioGitlab', optional: true
  scope :actualizado_entre, ->(start_date, end_date) { where('mergeado between ? AND ? OR creado between ? AND ?', start_date, end_date, start_date, end_date) }
end

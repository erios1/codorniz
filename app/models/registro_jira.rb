# == Schema Information
#
# Table name: registro_jiras
#
#  id              :bigint           not null, primary key
#  codigo          :text
#  fecha_inicio    :date
#  fecha_termino   :date
#  informante      :text
#  nombre          :text
#  prioridad       :text
#  resolucion      :text
#  responsable     :text
#  status          :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  issue_type_id   :bigint
#  project_id      :bigint
#  tablero_jira_id :integer
#
# Indexes
#
#  index_rj_on_tablero_jira_id  (tablero_jira_id)
#
class RegistroJira < ApplicationRecord
  belongs_to :tablero_jira
  belongs_to :project, class_name: 'ProyectoJira', optional: true
  belongs_to :issue_type, class_name: 'IssuesTypeJira', optional: true
end

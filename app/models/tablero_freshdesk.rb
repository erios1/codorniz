# == Schema Information
#
# Table name: tablero_freshdesks
#
#  id         :bigint           not null, primary key
#  generacion :date
#  nombre     :text
#  pais       :text
#  registros  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class TableroFreshdesk < ApplicationRecord

  has_many :registro_freshdesks, dependent: :destroy, autosave: true
end

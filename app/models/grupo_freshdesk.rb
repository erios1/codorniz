# == Schema Information
#
# Table name: grupo_freshdesks
#
#  id          :bigint           not null, primary key
#  descripcion :text
#  nombre      :text
#  tipo        :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class GrupoFreshdesk < ApplicationRecord

  has_many :agente_freshdesks, dependent: :destroy, autosave: true
  has_many :registro_freshdesk, foreign_key: :equipo_id, class_name: 'RegistroFreshdesk', inverse_of: :equipo, dependent: :restrict_with_error
end

# == Schema Information
#
# Table name: tablero_jiras
#
#  id         :bigint           not null, primary key
#  generacion :date
#  nombre     :text
#  pais       :text
#  registros  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class TableroJira < ApplicationRecord

  has_many :registro_jiras, dependent: :destroy, autosave: true
end

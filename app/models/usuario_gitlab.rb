# == Schema Information
#
# Table name: usuario_gitlabs
#
#  id             :bigint           not null, primary key
#  estado         :text
#  nombre         :text
#  nombre_usuario :text
#  web_url        :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
class UsuarioGitlab < ApplicationRecord
  has_many    :registro_authors, foreign_key: :author_id, class_name: 'RegistroGitlab', inverse_of: :author, dependent: :restrict_with_error
  has_many    :registro_mergeds, foreign_key: :merge_by_id, class_name: 'RegistroGitlab', inverse_of: :merge_by, dependent: :restrict_with_error
end

# == Schema Information
#
# Table name: agente_freshdesks
#
#  id                 :bigint           not null, primary key
#  email              :text
#  estado             :text
#  nombre             :text
#  telefono           :text
#  tipo               :text
#  titulo_trabajo     :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  grupo_freshdesk_id :bigint
#
class AgenteFreshdesk < ApplicationRecord
  belongs_to :grupo_freshdesk, optional: true
  has_many   :registro_freshdesks, foreign_key: :agente_id, class_name: 'RegistroFreshdesk', inverse_of: :agente, dependent: :restrict_with_error

  def usuario_gitlab
    UsuarioGitlab.find_by(nombre: nombre)
  end
end

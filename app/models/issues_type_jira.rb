# == Schema Information
#
# Table name: issues_type_jiras
#
#  id          :bigint           not null, primary key
#  description :text
#  name        :text
#  subtask     :boolean
#  tipo        :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  project_id  :bigint
#
class IssuesTypeJira < ApplicationRecord
  has_many    :registro_jiras, foreign_key: :issue_type_id, class_name: 'RegistroJira', inverse_of: :issue_type, dependent: :restrict_with_error
end

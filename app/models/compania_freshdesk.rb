# == Schema Information
#
# Table name: compania_freshdesks
#
#  id         :bigint           not null, primary key
#  country    :text
#  domain     :text
#  name       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class CompaniaFreshdesk < ApplicationRecord
end

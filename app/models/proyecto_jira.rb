# == Schema Information
#
# Table name: proyecto_jiras
#
#  id         :bigint           not null, primary key
#  codigo     :text
#  nombre     :text
#  type_key   :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class ProyectoJira < ApplicationRecord
  has_many    :registro_jiras, foreign_key: :project_id, class_name: 'RegistroJira', inverse_of: :project, dependent: :restrict_with_error
end

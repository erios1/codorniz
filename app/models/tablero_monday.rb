# == Schema Information
#
# Table name: tablero_mondays
#
#  id         :bigint           not null, primary key
#  generacion :date
#  nombre     :text
#  pais       :text
#  registros  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class TableroMonday < ApplicationRecord

  has_many :registro_tablero_mondays, dependent: :destroy, autosave: true
end

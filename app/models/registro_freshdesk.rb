# == Schema Information
#
# Table name: registro_freshdesks
#
#  id                            :bigint           not null, primary key
#  bloqueante                    :boolean
#  cliente                       :text
#  estado                        :text
#  limite_primera_respuesta_date :datetime
#  prioridad                     :text
#  resolved_date                 :datetime
#  tipo                          :text
#  titulo                        :text
#  url                           :text
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  agente_id                     :bigint
#  equipo_id                     :bigint
#  informante_id                 :bigint
#  tablero_freshdesk_id          :bigint
#
# Indexes
#
#  index_rf_on_tablero_freshdesk_id  (tablero_freshdesk_id)
#
class RegistroFreshdesk < ApplicationRecord
  belongs_to :tablero_freshdesk
  belongs_to :agente, class_name: 'AgenteFreshdesk', optional: true
  belongs_to :equipo, class_name: 'GrupoFreshdesk', optional: true
  belongs_to :informante, class_name: 'ContactoFreshdesk', optional: true


  scope :terminados, -> {where(estado: "Cerrado") }
  scope :abiertos, -> {where(estado: "Abierto") }
  scope :urgentes, -> {where(prioridad: "Urgente") }
  scope :altos, -> {where(prioridad: "Alta") }
  scope :medios, -> {where(prioridad: "Media") }
  scope :bajos, -> {where(prioridad: "Baja") }
  scope :actualizado_entre, ->(start_date, end_date) { where('updated_at::date between ? AND ?', start_date, end_date) }
  def cliente_url
    RegistroTableroMonday.find_by(url: cliente)&.url
  end
end

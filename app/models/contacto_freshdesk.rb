# == Schema Information
#
# Table name: contacto_freshdesks
#
#  id         :bigint           not null, primary key
#  email      :text
#  estado     :text
#  nombre     :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class ContactoFreshdesk < ApplicationRecord
  has_many    :registro_freshdesks, foreign_key: :informante_id, class_name: 'RegistroFreshdesk', inverse_of: :informante, dependent: :restrict_with_error
end

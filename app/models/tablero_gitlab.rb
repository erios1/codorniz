# == Schema Information
#
# Table name: tablero_gitlabs
#
#  id         :bigint           not null, primary key
#  generacion :date
#  nombre     :text
#  registros  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class TableroGitlab < ApplicationRecord

  has_many :registro_gitlabs, dependent: :destroy, autosave: true
end

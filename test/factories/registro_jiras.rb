# == Schema Information
#
# Table name: registro_jiras
#
#  id              :bigint           not null, primary key
#  codigo          :text
#  fecha_inicio    :date
#  fecha_termino   :date
#  informante      :text
#  nombre          :text
#  prioridad       :text
#  resolucion      :text
#  responsable     :text
#  status          :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  issue_type_id   :bigint
#  project_id      :bigint
#  tablero_jira_id :integer
#
# Indexes
#
#  index_rj_on_tablero_jira_id  (tablero_jira_id)
#
FactoryBot.define do
  factory :registro_jira do
    
  end
end

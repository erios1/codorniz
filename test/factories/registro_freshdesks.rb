# == Schema Information
#
# Table name: registro_freshdesks
#
#  id                            :bigint           not null, primary key
#  bloqueante                    :boolean
#  cliente                       :text
#  estado                        :text
#  limite_primera_respuesta_date :datetime
#  prioridad                     :text
#  resolved_date                 :datetime
#  tipo                          :text
#  titulo                        :text
#  url                           :text
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  agente_id                     :bigint
#  equipo_id                     :bigint
#  informante_id                 :bigint
#  tablero_freshdesk_id          :bigint
#
# Indexes
#
#  index_rf_on_tablero_freshdesk_id  (tablero_freshdesk_id)
#
FactoryBot.define do
  factory :registro_freshdesk do
    
  end
end

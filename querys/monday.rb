# api playground
# https://proyecto-buk.monday.com/apps/playground
# recurso
resource = RestClient::Resource.new 'https://api.monday.com/v2'
# se tiene que entregar la query como un param
# para que se pueda conseguir la informacion
response = resource.get(headers = {"Content-Type": "application/json",  'Authorization': 'eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjExMDY0NjY2NywidWlkIjoxMjY5MDczOCwiaWFkIjoiMjAyMS0wNS0xOVQxNjo1Nzo1My41MjlaIiwicGVyIjoibWU6d3JpdGUiLCJhY3RpZCI6NTY4Mjg3OCwicmduIjoidXNlMSJ9.YWC25-1o-rYtLFFWGU-snmfs4PvEDU5yLKAe0fwwZbg', params: {query: "{ boards {name} }"} })
response = resource.get(headers = {"Content-Type": "application/json",  'Authorization': 'eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjExMDY0NjY2NywidWlkIjoxMjY5MDczOCwiaWFkIjoiMjAyMS0wNS0xOVQxNjo1Nzo1My41MjlaIiwicGVyIjoibWU6d3JpdGUiLCJhY3RpZCI6NTY4Mjg3OCwicmduIjoidXNlMSJ9.YWC25-1o-rYtLFFWGU-snmfs4PvEDU5yLKAe0fwwZbg', params: {query: "{ boards {groups {title} }}"} })
# conseguir informacion de master clientes
response = resource.get(headers = {"Content-Type": "application/json", 'Authorization': 'eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjExMDY0NjY2NywidWlkIjoxMjY5MDczOCwiaWFkIjoiMjAyMS0wNS0xOVQxNjo1Nzo1My41MjlaIiwicGVyIjoibWU6d3JpdGUiLCJhY3RpZCI6NTY4Mjg3OCwicmduIjoidXNlMSJ9.YWC25-1o-rYtLFFWGU-snmfs4PvEDU5yLKAe0fwwZbg', params: {query: "{ boards (ids: 466188439){name items {name column_values {id text}}} }"} }) 
# ids conversion
# items name = nombre proyecto
# n__emp_ = numero empleados (tamaño empresa)
# etiquetas1 = etiqueta tamaño (grande,pequeña, micro) (tamaño empresa)
# nombre_pagina = nombre pagina (url)
# proyecto = estado
# jefe_proyecto1 = jefe de proyecto
# jefe_proyecto0 = backup jefe de proyecto
# (fecha de hoy o fecha termino) - fecha inicio = duracion
# kam4 = agente comercial
# estado = critico
# n_meros_17 = etapa
# modulos = modulos
# fecha_inicio = fecha inicio
# fecha_termino = fecha termino
# texto_1 = contraparte implementacion (en caso de)
# texto_largo = email contraparte (en caso de necesitar)
author = "eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjExMDY0NjY2NywidWlkIjoxMjY5MDczOCwiaWFkIjoiMjAyMS0wNS0xOVQxNjo1Nzo1My41MjlaIiwicGVyIjoibWU6d3JpdGUiLCJhY3RpZCI6NTY4Mjg3OCwicmduIjoidXNlMSJ9.YWC25-1o-rYtLFFWGU-snmfs4PvEDU5yLKAe0fwwZbg"
query = "{ boards (ids: 466188439){name items {name column_values (ids: [n__emp_ etiquetas1 nombre_pagina proyecto jefe_proyecto1 jefe_proyecto0 kam4 estado n_meros_17 modulos fecha_inicio fecha_termino texto_1 texto_largo]){id text}}} }"
header = {"Content-Type": "application/json", 'Authorization': author, params: {query: query} }
response = resource.get(header)
results_query = JSON.parse(response.to_str)
# llegar a arreglo de paginas
results_query["data"]["boards"]
# tomar nombre del primer registro
results_query["data"]["boards"][0]["name"]
# llegar a arreglo atributos del primer registro
results_query["data"]["boards"][0]["items"][0]
# comenzar a consultar por columnas
results_query["data"]["boards"][0]["items"][0]["column_values"]

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_07_09_195750) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "agente_freshdesks", force: :cascade do |t|
    t.text "tipo"
    t.text "estado"
    t.text "email"
    t.text "titulo_trabajo"
    t.text "telefono"
    t.text "nombre"
    t.bigint "grupo_freshdesk_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "compania_freshdesks", force: :cascade do |t|
    t.text "name"
    t.text "domain"
    t.text "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contacto_freshdesks", force: :cascade do |t|
    t.text "nombre"
    t.text "email"
    t.text "estado"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "grupo_freshdesks", force: :cascade do |t|
    t.text "nombre"
    t.text "descripcion"
    t.text "tipo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "issues_type_jiras", force: :cascade do |t|
    t.text "description"
    t.text "name"
    t.boolean "subtask"
    t.text "tipo"
    t.bigint "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "proyecto_jiras", force: :cascade do |t|
    t.text "codigo"
    t.text "nombre"
    t.text "type_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "registro_freshdesks", force: :cascade do |t|
    t.text "url"
    t.bigint "informante_id"
    t.text "tipo"
    t.text "estado"
    t.text "prioridad"
    t.bigint "agente_id"
    t.bigint "equipo_id"
    t.text "titulo"
    t.boolean "bloqueante"
    t.datetime "resolved_date"
    t.datetime "limite_primera_respuesta_date"
    t.bigint "tablero_freshdesk_id"
    t.text "cliente"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tablero_freshdesk_id"], name: "index_rf_on_tablero_freshdesk_id"
  end

  create_table "registro_gitlabs", force: :cascade do |t|
    t.text "nombre_mr"
    t.text "descripcion"
    t.text "estado"
    t.date "creado"
    t.date "mergeado"
    t.bigint "merge_by_id"
    t.text "rama_destino"
    t.text "nombre_rama"
    t.bigint "author_id"
    t.text "revisores"
    t.text "web_url"
    t.bigint "tablero_gitlab_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tablero_gitlab_id"], name: "index_rg_on_tablero_gitlab_id"
  end

  create_table "registro_jiras", force: :cascade do |t|
    t.text "codigo"
    t.text "resolucion"
    t.text "prioridad"
    t.text "responsable"
    t.text "status"
    t.text "informante"
    t.bigint "issue_type_id"
    t.bigint "project_id"
    t.text "nombre"
    t.date "fecha_termino"
    t.date "fecha_inicio"
    t.integer "tablero_jira_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tablero_jira_id"], name: "index_rj_on_tablero_jira_id"
  end

  create_table "registro_tablero_mondays", force: :cascade do |t|
    t.text "nombre"
    t.integer "n_empleados"
    t.text "tamanno"
    t.text "url"
    t.text "estado"
    t.text "jefe_proyecto"
    t.text "jefe_proyecto_backup"
    t.text "duracion"
    t.text "agente_comercial"
    t.text "criticidad"
    t.text "etapa"
    t.text "modulos"
    t.date "fecha_inicio"
    t.date "fecha_termino"
    t.text "contraparte"
    t.text "email_contraparte"
    t.integer "tablero_monday_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tablero_monday_id"], name: "index_rtm_on_tablero_monday_id"
  end

  create_table "tablero_freshdesks", force: :cascade do |t|
    t.text "nombre"
    t.date "generacion"
    t.text "pais"
    t.integer "registros"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tablero_gitlabs", force: :cascade do |t|
    t.text "nombre"
    t.date "generacion"
    t.integer "registros"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tablero_jiras", force: :cascade do |t|
    t.text "nombre"
    t.date "generacion"
    t.text "pais"
    t.integer "registros"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tablero_mondays", force: :cascade do |t|
    t.text "nombre"
    t.date "generacion"
    t.text "pais"
    t.integer "registros"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "usuario_gitlabs", force: :cascade do |t|
    t.text "nombre"
    t.text "nombre_usuario"
    t.text "estado"
    t.text "web_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end

class CreateUsuarioGitlabs < ActiveRecord::Migration[5.2]
  def change
    create_table :usuario_gitlabs do |t|
      t.text                  :nombre
      t.text                  :nombre_usuario
      t.text                  :estado
      t.text                  :web_url

      t.timestamps
    end
  end
end

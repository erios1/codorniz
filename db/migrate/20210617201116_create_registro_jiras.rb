class CreateRegistroJiras < ActiveRecord::Migration[5.2]
  def change
    create_table :registro_jiras do |t|
      t.text            :codigo #key
      t.text            :resolucion # fields, resolution, name
      t.text            :prioridad #fields, priority, name
      t.text            :responsable #fields, assigne, displayname (accountId)
      t.text            :status #field, status, name
      t.text            :informante #fields, creator, displayname
      t.bigint          :issue_type_id #fields, issuetype, id
      t.bigint          :project_id # fields, project, id
      t.text            :nombre #fields, summary
      t.date            :fecha_termino #fields, statuscategorychangedate
      t.date            :fecha_inicio #fields,  created
      t.integer         :tablero_jira_id
      t.index [:tablero_jira_id], name: :index_rj_on_tablero_jira_id, unique: false

      t.timestamps
    end
  end
end

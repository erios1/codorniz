class CreateRegistroFreshdesks < ActiveRecord::Migration[5.2]
  def change
    create_table :registro_freshdesks do |t|
      t.text            :url
      t.bigint          :informante_id
      t.text            :tipo
      t.text            :estado
      t.text            :prioridad
      t.bigint          :agente_id
      t.bigint          :equipo_id
      t.text            :titulo
      t.boolean         :bloqueante
      t.datetime        :resolved_date
      t.datetime        :limite_primera_respuesta_date
      t.bigint          :tablero_freshdesk_id
      t.text            :cliente
      t.index [:tablero_freshdesk_id], name: :index_rf_on_tablero_freshdesk_id, unique: false

      t.timestamps
    end
  end
end

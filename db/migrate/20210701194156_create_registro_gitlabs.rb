class CreateRegistroGitlabs < ActiveRecord::Migration[5.2]
  def change
    create_table :registro_gitlabs do |t|
      t.text            :nombre_mr
      t.text            :descripcion #optativo
      t.text            :estado
      t.date            :creado
      t.date            :mergeado
      t.bigint          :merge_by_id
      t.text            :rama_destino
      t.text            :nombre_rama
      t.bigint          :author_id
      t.text            :revisores
      t.text            :web_url
      t.bigint          :tablero_gitlab_id
      t.index [:tablero_gitlab_id], name: :index_rg_on_tablero_gitlab_id, unique: false

      t.timestamps
    end
  end
end

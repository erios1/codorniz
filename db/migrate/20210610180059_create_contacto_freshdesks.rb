class CreateContactoFreshdesks < ActiveRecord::Migration[5.2]
  def change
    create_table :contacto_freshdesks do |t|
      t.text            :nombre
      t.text            :email
      t.text            :estado

      t.timestamps
    end
  end
end

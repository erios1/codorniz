class CreateCompaniaFreshdesks < ActiveRecord::Migration[5.2]
  def change
    create_table :compania_freshdesks do |t|
      t.text        :name
      t.text        :domain
      t.text        :country

      t.timestamps
    end
  end
end

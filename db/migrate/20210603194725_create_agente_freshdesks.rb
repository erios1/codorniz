class CreateAgenteFreshdesks < ActiveRecord::Migration[5.2]
  def change
    create_table :agente_freshdesks do |t|
      t.text            :tipo
      t.text            :estado
      t.text            :email
      t.text            :titulo_trabajo
      t.text            :telefono
      t.text            :nombre
      t.bigint          :grupo_freshdesk_id

      t.timestamps
    end
  end
end

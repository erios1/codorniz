class CreateTableroGitlabs < ActiveRecord::Migration[5.2]
  def change
    create_table :tablero_gitlabs do |t|
      t.text    :nombre
      t.date    :generacion
      t.integer :registros

      t.timestamps
    end
  end
end

class CreateIssuesTypeJiras < ActiveRecord::Migration[5.2]
  def change
    create_table :issues_type_jiras do |t|
      t.text        :description
      t.text        :name
      t.boolean     :subtask
      t.text        :tipo
      t.bigint      :project_id
      
      t.timestamps
    end
  end
end

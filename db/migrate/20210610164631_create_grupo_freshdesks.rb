class CreateGrupoFreshdesks < ActiveRecord::Migration[5.2]
  def change
    create_table :grupo_freshdesks do |t|
      t.text            :nombre
      t.text            :descripcion
      t.text            :tipo

      t.timestamps
    end
  end
end

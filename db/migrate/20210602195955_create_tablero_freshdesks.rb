class CreateTableroFreshdesks < ActiveRecord::Migration[5.2]
  def change
    create_table :tablero_freshdesks do |t|
      t.text    :nombre
      t.date    :generacion
      t.text    :pais
      t.integer :registros

      t.timestamps
    end
  end
end
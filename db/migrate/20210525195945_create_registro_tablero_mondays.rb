class CreateRegistroTableroMondays < ActiveRecord::Migration[5.2]
  def change
    create_table :registro_tablero_mondays do |t|
      t.text            :nombre
      t.integer         :n_empleados
      t.text            :tamanno
      t.text            :url
      t.text            :estado
      t.text            :jefe_proyecto
      t.text            :jefe_proyecto_backup
      t.text            :duracion
      t.text            :agente_comercial
      t.text            :criticidad
      t.text            :etapa
      t.text            :modulos
      t.date            :fecha_inicio
      t.date            :fecha_termino
      t.text            :contraparte
      t.text            :email_contraparte
      t.integer         :tablero_monday_id
      t.index [:tablero_monday_id], name: :index_rtm_on_tablero_monday_id, unique: false

      t.timestamps
    end
  end
end

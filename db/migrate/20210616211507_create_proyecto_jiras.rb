class CreateProyectoJiras < ActiveRecord::Migration[5.2]
  def change
    create_table :proyecto_jiras do |t|
      t.text              :codigo
      t.text              :nombre
      t.text              :type_key
 
      t.timestamps
    end
  end
end

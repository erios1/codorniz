source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.0'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  # Funciones para crear modelos
  gem 'factory_bot'
  # y su integración con rails
  gem 'factory_bot_rails'
  # Imprimir objetos de forma legible.
  gem 'awesome_print'
  # Permite ejecutar tests en paralelos: revisar si es necesario en rails 6 que tiene su manera de hacerlo
  gem 'parallel_tests'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Una consola/repl mucho más potente
  gem 'pry'
  # Permite debuggear usando pry
  gem 'pry-byebug'
  # Permite ver documentación en pry
  gem 'pry-doc'
  # Configuración de rails para que la consola use pry
  gem 'pry-rails'
  # Para debuggear de donde se llaman consultas
  gem 'active_record_query_trace'
  # Hace análisis de seguridad del código
  gem 'brakeman', require: false
  # Documenta los campos de una tabla en el modelo
  gem 'annotate'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Eficienta el desarrollo al hacer que prenda más rapido rails/rake
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15', '< 4.0'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Cliente REST para consumir apis externas
gem "rest-client", "~> 2.1"

# Excel version nueva xlsx
gem 'rubyXL'


# Convierte strings vacíos en nil antes de guardar el modelo
gem 'nilify_blanks'

# Insercion masiva de lineas
gem 'activerecord-import', '~> 1.0'